const functions = require('firebase-functions');
const http = require('http');
const https = require('https');
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const express = require('express');
const cors = require('cors');
const bodyParser = require("body-parser");
var morgan = require('morgan');
var request = require('request');

var allowCrossDomain = function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
}

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan('combined'));
app.use(allowCrossDomain);


// Automatically allow cross-origin requests
app.use(cors({ origin: true }));

// Add middleware to authenticate requests

// build multiple CRUD interfaces:
app.get('/:id', (req, res) => {

  console.log('req', req.params.id);
  const id = req.params.id;
  txtFile = `https://pruebas-parcial2.s3-us-west-2.amazonaws.com/mutants/com.evancharlton.mileage-mutant${id}/errortrace.txt`;

  const options = {

    headers: {
      'Content-Type': 'text/plain'
    }
  };
  request.get(txtFile, options, function (err, res2, body) {
    console.log(res2)
    if (err) {
      console.error(err);
      res.send(err);
    }
    else if (res2.statusCode === 200) {
      console.log('allgood');
      res.status(200).send(body);
    } else {
      res.send(res2);

    }
  });

});


// Expose Express API as a single Cloud Function:
exports.api = functions.https.onRequest(app);

// launch our backend into a port
app.listen(3001, () => console.log(`LISTENING ON PORT 3001`));
