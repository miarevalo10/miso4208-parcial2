import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MutationTableComponent } from './mutation-table/mutation-table.component';
import { MutationAppDetailComponent } from './mutation-app-detail/mutation-app-detail.component';
import { AngularFirestore, AngularFirestoreModule } from 'angularfire2/firestore';
import { FirestoreService } from './firestore.service';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment.prod';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatIconModule} from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';

import {NgbCarouselModule} from '@ng-bootstrap/ng-bootstrap';

import { TestStrategyComponent } from './test-strategy/test-strategy.component';
import { VrtComponent } from './vrt/vrt.component';

const routes: Routes = [
  { path: ':id', component: MutationAppDetailComponent},
  { path: '', component: TestStrategyComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    MutationTableComponent,
    MutationAppDetailComponent,
    TestStrategyComponent,
    VrtComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatTableModule,
    MatButtonModule,
    MatPaginatorModule,
    MatCardModule,
    MatDividerModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatIconModule,
    NgbCarouselModule,
    MatInputModule
  ],
  providers: [ AngularFirestore,
    FirestoreService],
  bootstrap: [AppComponent],
  exports: [RouterModule]

})
export class AppModule { }
