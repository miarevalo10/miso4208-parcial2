import { Component, OnInit, ViewChild } from '@angular/core';
import { FirestoreService } from '../firestore.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';

export interface MutantData {
  number: string;
  detectionType: string;
  folderS3: string;
}
@Component({
  selector: 'app-mutation-table',
  templateUrl: './mutation-table.component.html',
  styleUrls: ['./mutation-table.component.css']
})
export class MutationTableComponent implements OnInit {

  mutantes = [];

  displayedColumns: string[] = ['number', 'type'];
  dataSource: MatTableDataSource<MutantData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  isLoading = true;

  constructor(private firestore: FirestoreService, private router: Router) {

    this.firestore.getMutants().subscribe( mutants => {
      mutants.forEach( mut => {
        this.mutantes.push(mut.payload.doc.data());
      });
      this.dataSource = new MatTableDataSource(this.mutantes);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
      console.error(error);
    });
  }

  ngOnInit() {
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
