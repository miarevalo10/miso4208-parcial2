
const fs = require("mz/fs");
const dotenv = require('dotenv');
dotenv.config()
const admin = require('firebase-admin');
const functions = require('firebase-functions');
const _ = require('lodash');
;
const FOLDER = '../../mutants/';
const BASE_URL = 'https://pruebas-parcial2.s3-us-west-2.amazonaws.com/mutants/com.evancharlton.mileage-mutant'
const APK_NAME = 'com.evancharlton.mileage_3110-aligned-debugSigned.apk';

const fbreport = [];

const TRIVIAL = 'TRIVIAL';
const RIP = 'RIP';
const VRT = 'VRT';

admin.initializeApp();

var db = admin.firestore();


uploadAppsFb();
function uploadAppsFb() {
    files = fs.readdirSync(FOLDER);
    console.time();
    const trivial = JSON.parse(fs.readFileSync('stillborn.json'));
    const rip = JSON.parse(fs.readFileSync('ripDetected0.json'));
    const rip2 = JSON.parse(fs.readFileSync('ripDetected1.json'));
    const ripall = rip.concat(rip2);
    var docCol = db.collection('mutantes');

    for (const file of files) {
        if (fs.lstatSync(FOLDER + file).isDirectory()) {
            const number = file.split('com.evancharlton.mileage-mutant')[1];
            var detectionType = '';
            if (_.includes(trivial,file)) {
                detectionType = TRIVIAL;
            } else if (_.includes(ripall,file)) {
                detectionType = RIP;
            } else {
                detectionType = VRT;
            }

            const mutant = {
                number: number,
                folderS3: `${BASE_URL}${number}`,
                detectionType: detectionType
            }

           // fbreport.push(mutant);

            docCol.doc(number).set(mutant)
            .then(data => {console.log('done', data)})
            .catch(error => {console.error(error)});
        }
    }

   // fs.writeFileSync('dbreport.json', JSON.stringify(fbreport), 'utf8');
}
//chunk1();
function chunk1() {
    console.time();

    files = fs.readdirSync(FOLDER);

    const chunk1 = _.chunk(files, files.length / 8)[0];

    fs.writeFileSync('chunk11111.json', JSON.stringify(chunk1), 'utf8');
}

//test();
function test() {
    var docRef = db.collection('mutantes').doc('2');

    docRef.set({
        apkUrl: `${BASE_URL}1/${APK_NAME}`,
    }).then(data => {console.log('done', data)})
    .catch(error => {console.error(error)})
}

function vrtDiff(mutant, diff) {
    var docRef = db.collection('mutantes').doc(mutant);

    docRef.set({
        vrtDiff: diff,
    }).then(data => {console.log('done', data)})
    .catch(error => {console.error(error)})
}