import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test-strategy',
  templateUrl: './test-strategy.component.html',
  styleUrls: ['./test-strategy.component.css']
})
export class TestStrategyComponent implements OnInit {

  playstore = 'https://play.google.com/store/apps/details?id=com.evancharlton.mileage&hl=en';
  apkUrl = 'https://pruebas-parcial2.s3-us-west-2.amazonaws.com/baseline/com.evancharlton.mileage_3110.apk';

  constructor() { }

  ngOnInit() {
  }

}
