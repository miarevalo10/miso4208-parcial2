import { Component, OnInit, Input } from '@angular/core';
import { MutantData } from '../mutation-table/mutation-table.component';
import { HttpService } from '../http.service';

export interface Diff {
  name: string,
  diff: any
}
@Component({
  selector: 'app-vrt',
  templateUrl: './vrt.component.html',
  styleUrls: ['./vrt.component.css']
})


 export class VrtComponent implements OnInit {

  @Input() mutant: MutantData;

  displayedColumns: string[] = ['original', 'mutante', 'diff'];


  baselineImg = [];
  mutantImg = [];

  imgs = [];

  BASE_URL = 'https://pruebas-parcial2.s3-us-west-2.amazonaws.com/baseline/test';

  isLoading = true;

  constructor(private http: HttpService) { }

  ngOnInit() {
    const { folderS3 } = this.mutant;
    this.http.getJson(`${folderS3}/vrt/vrtDiff.json`).subscribe((data: Array<Diff>) => {
      data.forEach(element => {
        const img = {
          original : `${this.BASE_URL}/${element.name}`,
          mutante: `${folderS3}/test/${element.name}`,
          diff: `${folderS3}/vrt/${element.name}`,
          info : element.diff.misMatchPercentage
        };
        this.imgs.push(img);
      });
      this.isLoading = false;

    }, error => {
      console.error(error);
      this.isLoading = false;

    });
    console.log(this.imgs);

  }

}
