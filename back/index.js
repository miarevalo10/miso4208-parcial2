const fs = require("mz/fs")
const shell = require('shelljs');
const path = require('path');
const execSync = require('child_process').execSync;
const _ = require('lodash');
const FOLDER = '../../mutants/';
const BASE_REPLAY = '../../test/result.json';

const APK_NAME = 'com.evancharlton.mileage_3110-aligned-debugSigned.apk';
const EXTENSION = '.png';
const adb = '~/Android/Sdk/platform-tools/./adb';
const aapt = '/home/maria/Android/Sdk/build-tools/27.0.3/aapt';
const package = 'com.evancharlton.mileage';
const main_activity = 'com.evancharlton.mileage.Mileage';

stillborn = [];
trivial = [];
vrt = [];

//TODO Primero descartar de manera más rápida si se tienen triviales solo con instalar la aplicación, y agregar a lista stillborn:
// Starting: Intent { act=android.intent.action.MAIN cat=[android.intent.category.LAUNCHER] cmp=com.evancharlton.mileage/.Mileage }
// --------- beginning of crash
// --------- beginning of system
// --------- beginning of main
//TODO Ajustar RIP para que no tenga que pedir package ni main activity y corra mas rápido, correr RIP solo en los que no sean stillborn,
// si no hay crash en toda la ejecución, el error debe ser del layout, entonces agregar a vrt
//TODO correr resemble con las que quedaron de vrt
// TODO llegó hasta 2780

runTrivialDetection();
function runTrivialDetection() {
    files = fs.readdirSync(FOLDER);
    const chunk = _.chunk(files, files.length / 4)[0];
    const doneTrivial = [];
    stillborn = JSON.parse(fs.readFileSync('stillborn.json'));

    console.time();
    for (const file of stillborn) {
        if (fs.lstatSync(FOLDER + file).isDirectory()) {
            console.log('TRIVIAL DETECTION OF', file);
            shell.exec(`${adb} install ${FOLDER}${file}/${APK_NAME}`)
            shell.exec(`${adb} shell am start ${package}/${main_activity}`);
            const { stdout } = shell.exec(`${adb} logcat -d ${package}:I *:S`);

            if (stdout.match('beginning of crash')) {
                console.log('TRIVIAL');
                shell.exec(`adb logcat -d AndroidRuntime:E *:S >  ${FOLDER}${file}/errortrace.txt`);
                stillborn.push(file)
            }
            shell.exec(`adb shell pm clear ${package}`);
            shell.exec(`${adb} uninstall ${package}`);
            shell.exec('adb logcat -c');
        }
        fs.writeFileSync('trivialreal.json', JSON.stringify(stillborn), 'utf8');
        doneTrivial.push(file);
        fs.writeFileSync('stillborndone.json', JSON.stringify(doneTrivial), 'utf8');
    }
    fs.writeFileSync('trivialreal.json', JSON.stringify(stillborn), 'utf8');
    console.timeEnd();
}

//runOneTrivialDetection(6);
function runOneTrivialDetection(number) {
    shell.exec(`${adb} install ${FOLDER}com.evancharlton.mileage-mutant${number}/${APK_NAME}`).stdout;
    shell.exec(`${adb} shell am start ${package}/${main_activity}`).stdout;
    //const {stdout, stderr} = shell.exec(`${adb} logcat -d ${package}:I *:S`);
    shell.exec(`${adb} logcat -d ${package}:I *:S  ${FOLDER}com.evancharlton.mileage-mutant${number}/errortrace.txt`);
    shell.exec(`adb shell pm clear ${package}`)
    shell.exec(`${adb} uninstall ${package}`).stdout;
    shell.exec('adb logcat -c');
}


runRIP();
function runRIP() {
    console.time();

    files = fs.readdirSync(FOLDER);
    stillborn = JSON.parse(fs.readFileSync('stillborn.json'));
    const ripdone =  JSON.parse(fs.readFileSync('ripdone2.json'));
    const chunk1 = _.chunk(files, files.length / 8)[1];
    const done = [];
    
    for (const file of chunk1) {
        if (fs.lstatSync(FOLDER + file).isDirectory() && !_.includes(stillborn, file) &&  !_.includes(ripdone, file)) {
            console.log('running rip on  ', file);
            shell.exec(`java -jar riplite.jar rip_config.json ${FOLDER}${file}/${APK_NAME} ${FOLDER}${file}/test`);
            const log = fs.readFileSync(`${FOLDER}${file}/test/log.log`, { encoding: 'utf8' });

            if (log.includes('Application Error')) {
                shell.exec(`adb logcat -d AndroidRuntime:E *:S >  ${FOLDER}${file}/errortrace.txt`);
                trivial.push(file);
                console.log('ERROR');
                fs.writeFileSync('trivial.json', JSON.stringify(trivial), 'utf8');
            } else {
                vrt.push(file);
                fs.writeFileSync('vrt.json', JSON.stringify(vrt), 'utf8');
            }
            shell.exec(`adb shell pm clear ${package}`);
            shell.exec(`adb uninstall ${package}`);
            shell.exec('adb logcat -c');
            done.push(file);
            fs.writeFileSync('ripdone.json', JSON.stringify(done), 'utf8');
        }

    }
    console.timeEnd();
    console.log('trivial', trivial);;
    fs.writeFileSync('trivial.json', JSON.stringify(trivial), 'utf8');
}

//runOne(1);
//runOne(1520);

function runOne(number) {
    console.time();
    shell.exec(`java -jar RIPRR.jar rip_config.json ${FOLDER}com.evancharlton.mileage-mutant${number}/${APK_NAME} ${FOLDER}com.evancharlton.mileage-mutant${number}/test ${BASE_REPLAY}`);
    const ripone = []
    const log = fs.readFileSync(`${FOLDER}com.evancharlton.mileage-mutant${number}/test/log.log`, { encoding: 'utf8' });

    if (log.includes('Application Error')) {
        shell.exec(`adb logcat -d AndroidRuntime:E *:S > ${FOLDER}com.evancharlton.mileage-mutant${number}/errortrace.txt`);
        //trivial.push(`com.evancharlton.mileage-mutant${number}`);
        //fs.writeFileSync('trivial.json', JSON.stringify(trivial), 'utf8');
    }
    console.timeEnd();

    shell.exec(`adb shell pm clear ${package}`);
    shell.exec(`adb uninstall ${package}`);
    shell.exec('adb logcat -c');

}

//runVRT();
function runVRT() {

    files = fs.readdirSync(FOLDER);

    for (const file of files) {
        if (fs.lstatSync(FOLDER + file).isDirectory()) {
            statesImgs = fs.readdirSync(FOLDER + file + '/test').filter(file => {
                return path.extname(file).toLowerCase() === EXTENSION && file.match('^[1-9][1-9]?');
            });
            console.log('Imágenes de mutante', file, statesImgs);
        }
    }
}

//report();
function report() {
    const chunk1 = JSON.parse(fs.readFileSync('chunk1.json'));
    const chunk2 = JSON.parse(fs.readFileSync('chunk2.json'));
    const chunk3 = JSON.parse(fs.readFileSync('chunk3.json'));
    const chunk4 = JSON.parse(fs.readFileSync('chunk4.json'));

    stillborn = chunk1.concat(chunk2).concat(chunk3).concat(chunk4);
    console.log(stillborn);
    fs.writeFileSync('stillborn.json', JSON.stringify(stillborn), 'utf8');
}
