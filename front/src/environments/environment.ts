// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCpFjKGvUO3jT4l-gy91ueXpPqX-KqgqvA",
    authDomain: "pruebas-parcial2.firebaseapp.com",
    databaseURL: "https://pruebas-parcial2.firebaseio.com",
    projectId: "pruebas-parcial2",
    storageBucket: "pruebas-parcial2.appspot.com",
    messagingSenderId: "729592715122",
    appId: "1:729592715122:web:decd8798a2592d60"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
