import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  cors = 'https://cors-anywhere.herokuapp.com/';

  constructor(private http: HttpClient) { }

  getFile(url) {
    const headers = new HttpHeaders().set('Content-Type', 'text/plain');
    return this.http.get(this.cors + url, {headers});
  }

  getJson(url) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(this.cors + url, {headers});
  }
}
