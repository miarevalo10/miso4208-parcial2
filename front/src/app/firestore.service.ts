import { Injectable } from '@angular/core';
import { AngularFirestore, CollectionReference, Action, DocumentSnapshot } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { MutantData } from './mutation-table/mutation-table.component';
import { Resolve, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class FirestoreService implements Resolve<Action<DocumentSnapshot<{}>>> {

  mutantes = [];

  constructor(private firestore: AngularFirestore) {
  }

  getMutants() {
    return this.firestore.collection('mutantes').snapshotChanges();
  }

  getMutant(id: string) {
    return this.firestore.collection('mutantes').doc(id).snapshotChanges();
  }

  resolve(route: ActivatedRouteSnapshot) {
    const id = route.paramMap.get('id');
    return this.getMutant(id);
  }
}
