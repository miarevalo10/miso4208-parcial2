import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MutationAppDetailComponent } from './mutation-app-detail.component';

describe('MutationAppDetailComponent', () => {
  let component: MutationAppDetailComponent;
  let fixture: ComponentFixture<MutationAppDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MutationAppDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MutationAppDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
