import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestStrategyComponent } from './test-strategy.component';

describe('TestStrategyComponent', () => {
  let component: TestStrategyComponent;
  let fixture: ComponentFixture<TestStrategyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestStrategyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestStrategyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
