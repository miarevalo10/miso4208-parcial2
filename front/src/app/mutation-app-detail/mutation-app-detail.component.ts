import { Component, OnInit } from '@angular/core';
import { MutantData } from '../mutation-table/mutation-table.component';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { FirestoreService } from '../firestore.service';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-mutation-app-detail',
  templateUrl: './mutation-app-detail.component.html',
  styleUrls: ['./mutation-app-detail.component.css']
})
export class MutationAppDetailComponent implements OnInit {

  APK_NAME = 'com.evancharlton.mileage_3110-aligned-debugSigned.apk';
  APK_BASELINE = 'com.evancharlton.mileage_3110.apk';
  ERROR_FILE = 'errortrace.txt';
  REPLAY_FILE = 'test/result.json';
  BASE_URL = 'https://pruebas-parcial2.s3-us-west-2.amazonaws.com/baseline/test';

  mutant;
  isLoading = true;
  baseLine = false;

  apkUrl = '';
  errorUrl = '';
  riprr = '';

  errorTxt = '';
  imgs = [];
  mutImgs = [];

  constructor(private route: ActivatedRoute, private firestore: FirestoreService, private http: HttpService) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    this.firestore.getMutant(id).subscribe(mutantfs => {

      this.mutant = mutantfs.payload.data();
      this.apkUrl = `${this.mutant.folderS3}/${this.APK_NAME}`;
      this.isLoading = false;
      this.errorUrl = `${this.mutant.folderS3}/${this.ERROR_FILE}`;
      this.riprr = `${this.mutant.folderS3}/${this.REPLAY_FILE}`;

      if (this.mutant.detectionType === 'BASELINE') {
        this.baseLine = true;
        this.apkUrl = `${this.mutant.folderS3}/${this.APK_BASELINE}`;
        var i = 1;

        for (i; i < 9; i++) {
          this.imgs.push(`${this.BASE_URL}/${i}.png`);
        }
      } else if (this.mutant.detectionType === 'TRIVIAL' || this.mutant.detectionType === 'RIP') {
        this.http.getFile(this.errorUrl).subscribe(data => {
          console.log('data', data);
        }, error => {
          this.errorTxt = error.error.text;
        });
        for (i; i < 9; i++) {
          this.mutImgs.push(`${this.mutant.folderS3}/test/${i}.png`);
        }

      }
      console.log('MUTANTE ESCOGIDO', this.mutant);

    }, error => {
      console.error(error);
      this.isLoading = false;
    });

  }

}
