var AWS = require('aws-sdk');
const compareImages = require("resemblejs/compareImages");
const admin = require('firebase-admin');
const fs = require("mz/fs");
const dotenv = require('dotenv');
dotenv.config();
const _ = require('lodash');
admin.initializeApp();

var db = admin.firestore();

AWS.config.update({
    region: 'us-west-2',
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});

AWS.config.update({ region: 'us-west-2' });
var s3 = new AWS.S3();

const BUCKET_NAME = 'pruebas-parcial2';
const URL_S3 = 'https://s3-us-west-2.amazonaws.com/' + BUCKET_NAME + "/mutants"
const BASE_FOLDER = '../../test';
const MUTANT_FOLDER = '../../mutants';


const filetest = `com.evancharlton.mileage-mutant1`;
//runVrt(file);
//runVrtAll()
function runVrtAll() {

    files = fs.readdirSync(MUTANT_FOLDER);
    vrts = JSON.parse(fs.readFileSync('vrt.json'));
    vrtDone = [];
    const chunk = _.chunk(vrts, files.length/10)[0] ;

    for (const file of chunk) {
        if (fs.lstatSync(MUTANT_FOLDER + '/'+ file).isDirectory() && _.includes(vrts, file)) {
            console.log('VRT ASD', file);
            runVrt(file);
            vrtDone.push(file);
            fs.writeFileSync('vrtdone1.json', JSON.stringify(vrtDone), 'utf8');
        }
    }
}

function runVrt(file) {
    console.time();
    var imgs = [];
    i = 1;
    for (i; i < 9; i++) {
        name = `${i}.png`
        const img = {
            name: name,
            original: `${BASE_FOLDER}/${name}`,
            mutante: `${MUTANT_FOLDER}/${file}/test/${name}`,
            diff: `${MUTANT_FOLDER}/${file}/vrt/${name}`
        };
        imgs.push(img);
    }
    const number = file.split('com.evancharlton.mileage-mutant')[1];

    createVRTFolder(file)
    compareAllImages(imgs, file, number);
    console.timeEnd();
}

function createVRTFolder(file) {
    const vrtPath = `${MUTANT_FOLDER}/${file}/vrt`
    if (!fs.existsSync(vrtPath)) {
        fs.mkdirSync(vrtPath);
    }
}

function compareAllImages(imgs, file) {

    diffData = [];
    let promises = [];

    imgs.forEach(img => {
        const s3key = `mutants/${file}/vrt/${img.name}`;
        promises.push(getDiff(img.original, img.mutante, img.diff, s3key).then(data => {
            diffData.push({ name: img.name, diff: data });
        }));
    });

    Promise.all(promises).then(_ => {
        addDiffData(file, diffData)
    });
}

function addDiffData(file, diff) {
    fs.writeFileSync(`${MUTANT_FOLDER}/${file}/vrt/vrtDiff.json`, JSON.stringify(diff), 'utf8');
    uploadFile(`${MUTANT_FOLDER}/${file}/vrt/vrtDiff.json` ,  `mutants/${file}/vrt/vrtDiff.json`,'application/json');
}

async function getDiff(img1, img2, output, s3key) {

    const options = {
        output: {
            errorColor: {
                red: 255,
                green: 0,
                blue: 255
            },
            errorType: "movement",
            transparency: 0.3,
            largeImageThreshold: 1200,
            useCrossOrigin: false,
            outputDiff: true
        },
        scaleToSameSize: true,
        ignore: "less"
    };

    const data = await compareImages(
        fs.readFileSync(img1),
        fs.readFileSync(img2),
        options
    );
    fs.writeFileSync(output, data.getBuffer());
    uploadFile(output, s3key, 'image/png');
    return data;
    /*
    {
      misMatchPercentage : 100, // %
      isSameDimensions: true, // or false
      dimensionDifference: { width: 0, height: -1 }, // defined if dimensions are not the same
      getImageDataUrl: function(){}
    }
    */

}

var uploadFile = function (file, s3key, contentType) {
    fs.readFile(file, function (err, data) {
        if (err) { throw err; }
        var params = {
            Bucket: 'pruebas-parcial2',
            Key: s3key,
            Body: data,
            ContentType: contentType,
            ACL: 'public-read'
        };

        return new Promise((resolve, reject) => {
            s3.putObject(params, function (err, data) {
                if (err) {
                    reject(err);
                } else {
                    console.log("Successfully uploaded data ", s3key);
                    resolve(data);
                }

            });
        });

    });
}


