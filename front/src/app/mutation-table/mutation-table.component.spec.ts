import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MutationTableComponent } from './mutation-table.component';

describe('MutationTableComponent', () => {
  let component: MutationTableComponent;
  let fixture: ComponentFixture<MutationTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MutationTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MutationTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
